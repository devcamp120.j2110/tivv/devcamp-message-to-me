import TitleImage from "./title-image/TitleImage";
import TitleText from "./title-text/TitleText";

function TitleComponent() {
    return (
        <>
            <TitleText />
            <TitleImage />
        </>
    )
}

export default TitleComponent;