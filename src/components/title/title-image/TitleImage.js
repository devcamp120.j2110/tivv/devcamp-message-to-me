import { Col, Row } from "reactstrap";
import titleImg from "../../../assets/images/title.png";

function TitleImage() {
    return (
        <Row style={{marginBottom: 10}}> 
            <Col>
                <img src={titleImg} alt="Title" width={500} className="img-thumbnail"/>
            </Col>
        </Row>
    )
}

export default TitleImage;