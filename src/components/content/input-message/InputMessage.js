import { Button, Col, Input, Label, Row } from "reactstrap";

function InputMessage({inputMessage, changeInputMessageHandler, changeMessageHandler}) {
    const onChangeInput = (event) => {
        console.log("Nhập vào ô input");
        let inputValue = event.target.value;
        console.log(inputValue);

        changeInputMessageHandler(inputValue);
    }

    const onClickBtn = () => {
        console.log("Bấm nút gửi thông điệp");
        console.log(inputMessage);

        changeMessageHandler(inputMessage);
    }

    return (
        <>
            <Row> 
                <Col>
                    <Label>Message cho bạn 12 tháng tới:</Label>
                </Col>
            </Row>
            <Row style={{marginBottom: 20}}> 
                <Col>
                    <Input value={inputMessage} onChange={onChangeInput} placeholder="Nhập message vào đây"></Input>
                </Col>
            </Row>
            <Row> 
                <Col>
                    <Button color="success" onClick={onClickBtn}>Gửi thông điệp</Button>
                </Col>
            </Row>
        </>
    )
}

export default InputMessage;