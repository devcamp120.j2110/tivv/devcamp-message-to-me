import { useState } from "react";
import InputMessage from "./input-message/InputMessage";
import LikeImage from "./like-image/LikeImage";

function ContentComponent() {
    const [inputMessage, setInputMessage] = useState("");
    const [message, setMessage] = useState([]);
    const [likeDisplay, setLikeDisplay] = useState("none");

    const changeInputMessage = (paramInputMessage) => {
        setInputMessage(paramInputMessage);
    }

    const changeMessage = (paramMessage) => {
        if(paramMessage) {
            setMessage([...message, paramMessage]);
            setLikeDisplay("block");
        }
    }


    return (
        <>
            <InputMessage inputMessage={inputMessage} changeInputMessageHandler={changeInputMessage} changeMessageHandler={changeMessage} />
            <LikeImage message={message} likeDisplay={likeDisplay} />
        </>
    )
}

export default ContentComponent;