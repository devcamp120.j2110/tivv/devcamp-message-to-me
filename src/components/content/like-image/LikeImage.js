import { Col, Row } from "reactstrap";
import likeImg from "../../../assets/images/like.png";

function LikeImage({ message, likeDisplay }) {
    return (
        <>
            <Row style={{marginTop: 30}}> 
                <Col>
                {message.map((element, index) => {
                    return <p key={index}>{element}</p>
                })}
                </Col>
            </Row>
            <Row> 
                <Col>
                    <img src={likeImg} alt="Like" width={200} className="img-thumbnail" style={{display: likeDisplay}}/>
                </Col>
            </Row>
        </>
    )
}

export default LikeImage;